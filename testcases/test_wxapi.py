import allure
import pytest

from common.send_reqursts import SendRequests
from common.yaml_utli import read_yaml_testcase, write_yaml, read_yaml


@allure.parent_suite("订个厢小程序端API")
@allure.suite("小程序端")
@allure.sub_suite("小程序端接口测试类")
class TestWxapi:
    @allure.title('获取城市id')
    @pytest.mark.parametrize('caseinfo', read_yaml_testcase('testcases/test_getCityid.yaml'))
    def test_getCityId(self, caseinfo):

        res = SendRequests().all_send_requests(method=caseinfo['request']['method'],
                                               url=caseinfo['request']['url'],
                                               params=caseinfo['request']['data'])
        write_yaml({"areaId": res.json()['result'][0]['id']})
        write_yaml({"city": res.json()['result'][0]['areaName']})
        # print("这是什么", caseinfo['status_code'])
        assert res.json()['result'][0]['id'] == caseinfo['validate']['status_code']


    @allure.title('获取首页数据')
    @pytest.mark.parametrize('caseinfo', read_yaml_testcase('testcases/test_getGoodStore.yaml'))
    def test_getGoodStore(self, caseinfo):
        datas = caseinfo['request']['data']
        datas['areaId'] = read_yaml('areaId')
        res = SendRequests().all_send_requests(method=caseinfo['request']['method'],
                                         url=caseinfo['request']['url'],
                                         params=datas)
        write_yaml({"latitude": res.json()['result']['records'][0]['latitude']})
        write_yaml({"longitude": res.json()['result']['records'][0]['longitude']})
        write_yaml({"shopName": res.json()['result']['records'][0]['shopName']})

    @allure.title('首页搜索商家')
    @pytest.mark.parametrize('caseinfo', read_yaml_testcase('testcases/test_getShopList.yaml'))
    def test_getShopList(self, caseinfo):
        datas = caseinfo['request']['data']
        datas['areaId'] = read_yaml('areaId')
        datas['city'] = read_yaml('city')
        datas['latitude'] = read_yaml('latitude')
        datas['longitude'] = read_yaml('longitude')
        datas['name'] = read_yaml('shopName')

        res = SendRequests().all_send_requests(method=caseinfo['request']['method'],
                                               url=caseinfo['request']['url'],
                                               params=datas)

    @allure.title('获取商家信息')
    @pytest.mark.parametrize('caseinfo', read_yaml_testcase('testcases/test_getShopInfo.yaml'))
    def test_getShopInfo(self, caseinfo):
        res = SendRequests().all_send_requests(method=caseinfo['request']['method'],
                                               url=caseinfo['request']['url'],
                                               params=caseinfo['request']['data'])

    @allure.title('查询店家所有包厢、卡座信息')
    @pytest.mark.parametrize('caseinfo', read_yaml_testcase('testcases/test_listAllForSelect.yaml'))
    def test_listAllForSelect(self, caseinfo):
        res = SendRequests().all_send_requests(method=caseinfo['request']['method'],
                                               url=caseinfo['request']['url'],
                                               params=caseinfo['request']['data'])

    #
    # def test_queryById(self):
    #     """通过id查询店家包厢、卡座信息"""
    #     print("""通过id查询店家包厢、卡座信息""")
    #     url = "http://api-test.haipingjia.cn/hpj-diningtable/customer/dining_table/diningTable/queryById.open"
    #     headers = {
    #         "Content-Type": "application/json",
    #         "X-Access-Token": TestWxapi.api_token
    #     }
    #     params = {
    #         "shopId": TestWxapi.api_shop_id,
    #         "userId": TestWxapi.api_user_id,
    #         "id": TestWxapi.api_id
    #     }
    #     res = SendRequests().all_send_requests(method="get", url=url, params=params, headers=headers)
    #     TestWxapi.api_diningAreaId = res.json()['result']["diningAreaId"]
    #     TestWxapi.api_diningTableTypeId = res.json()['result']["diningTableTypeId"]
    #     print("diningAreaId", TestWxapi.api_diningAreaId)

#     def test_cDishListMap(self):
#         """查看根据菜品分组返回列表"""
#         print("查看根据菜品分组返回列表")
#         url = "http://api-test.haipingjia.cn/hpj-dish/customer/dish/cDishListMap.open"
#         headers = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#         params = {
#             "shopId": TestWxapi.api_shop_id,
#             "userId": TestWxapi.api_user_id,
#             "id": TestWxapi.api_id
#         }
#         res = SendRequests().all_send_requests(method="get", url=url, params=params, headers=headers)
#         TestWxapi.api_dishGroupId = res.json()['result'][0]["cdishVoList"][0]["id"]
#         print("商品id", TestWxapi.api_dishGroupId)
#
#     def test_addGoods(self):
#         """商品加入购物车"""
#         print("商品加入购物车")
#         url = "http://api-test.haipingjia.cn/hpj-appointmentorder/customer/shopCar/addGoods"
#         headers = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#         datas = {
#             "areaId": TestWxapi.api_city_id,
#             "childDishLish": [
#                 {
#                     "dishId": TestWxapi.api_dishGroupId,
#                     "dishName": "红烧狮子头1",
#                     # "dishSpecs": [],
#                     "quantity": 1,
#                     "unitPrice": 9.9
#                 }
#             ],
#             # "diningTableId": "",
#             "dishId": TestWxapi.api_dishGroupId,
#             "dishName": "红烧狮子头1",
#             # "dishSpecsId": [],
#             "id": "",
#             "isAppointment": 0,
#             "quantity": 1,
#             "unitPrice": 9.9,
#             "userId": TestWxapi.api_user_id
#         }
#         res = SendRequests().all_send_requests(method="post", url=url, json=datas, headers=headers)
#
#     def test_queryShopCar(self):
#         """查询购物车商品"""
#         print("查询购物车商品")
#         url = "http://api-test.haipingjia.cn/hpj-appointmentorder/customer/shopCar/queryShopCar"
#         headers = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#         params = {
#             "shopId": TestWxapi.api_shop_id,
#             "areaId": TestWxapi.api_city_id,
#             "userId": TestWxapi.api_user_id,
#
#             "orderType": 0,
#             "pageSize": 9999,
#
#
#         }
#         res = SendRequests().all_send_requests(method="get", url=url, params=params, headers=headers)
#         TestWxapi.api_ShopCar_id = res.json()['result']['records'][0]["id"]
#         print("购物车ID", TestWxapi.api_ShopCar_id)
#
#     def test_addOrderByShopCar(self):
#         """购物车下单"""
#         print("购物车下单")
#         url = "http://api-test.haipingjia.cn/hpj-appointmentorder/customer/order/addOrderByShopCar"
#         headers = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#
#         datas = {
#
#
#
#             "areaId": TestWxapi.api_city_id,
#             "diningTableId": TestWxapi.api_id,
#             "diningAreaId": 1543876212071669761,
#             "diningAreaName": "东区",
#             "diningTableTypeId": TestWxapi.api_diningTableTypeId,
#
#             "mealTimePeriodBeginTime": "18:00",
#             "mealTimePeriodEndTime": "18:30",
#             "mealTimePeriodTime": TestWxapi.mealTimePeriodTime,
#             "mealTimeType": "晚餐",
#             "mealTimeTypeId": 741,
#             "orderPeopleNum": 5,
#             "shopCarIds": [TestWxapi.api_ShopCar_id],
#             "shopId": TestWxapi.api_shop_id,
#             "tableName": "包厢",
#             "tableType": 1
#         }
#         res = SendRequests().all_send_requests(method="post", url=url, json=datas, headers=headers)
#         TestWxapi.api_orderId_id = res.json()['result']["id"]
#         TestWxapi.api_operationNum_id = res.json()['result']["operationNum"]
#         print("订单ID：", TestWxapi.api_orderId_id)
#         print("操作ID：", TestWxapi.api_operationNum_id)
#
#     def test_confirmOrderDetail(self):
#         """查看订单确认详情"""
#         print("查看订单确认详情、确认下单")
#         url1 = "http://api-test.haipingjia.cn/hpj-appointmentorder/customer/order/confirmOrderDetail"
#         headers1 = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#         params1 = {
#             "orderId": TestWxapi.api_orderId_id,
#             "operationNum": TestWxapi.api_operationNum_id
#         }
#         res = SendRequests().all_send_requests(method="get", url=url1, params=params1, headers=headers1)
#
#         TestWxapi.api_contacts = res.json()['result']["customerPhone"]
#         print("联系人：", TestWxapi.api_contacts)
#         TestWxapi.api_contactsPhone = res.json()['result']["contacts"]
#         print("联系人电话：", TestWxapi.api_contactsPhone)
#         goodsGroupList = res.json()['result']["goodsGroupList"]
#         print(goodsGroupList)
#         print("订单确认------------------------------------------------------------------------------------------------")
#         url = "http://api-test.haipingjia.cn/hpj-appointmentorder/customer/order/addOrderByShopCar"
#         headers = {
#             "Content-Type": "application/json",
#             "X-Access-Token": TestWxapi.api_token
#         }
#         datas = {
#             "areaId": TestWxapi.api_city_id,
#             "diningTableId": TestWxapi.api_id,
#             "diningAreaId": 1543876212071669761,
#             "diningAreaName": "东区",
#             "diningTableTypeId": TestWxapi.api_diningTableTypeId,
#
#             "mealTimePeriodBeginTime": "18:00",
#             "mealTimePeriodEndTime": "18:30",
#             "mealTimePeriodTime": TestWxapi.mealTimePeriodTime,
#             "mealTimeType": "晚餐",
#             "mealTimeTypeId": 741,
#             "orderPeopleNum": 5,
#             "shopCarIds": [TestWxapi.api_ShopCar_id],
#             "shopId": TestWxapi.api_shop_id,
#             "tableName": "包厢",
#             "tableType": 1,
#
#
#             # "areaId": TestWxapi.api_city_id,
#             "contacts": res.json()['result']["contacts"],
#             "contactsPhone": res.json()['result']["customerPhone"],
#
#             "goodsGroupList": res.json()['result']["goodsGroupList"],
#
#             "operationNum": TestWxapi.api_operationNum_id,
#             "orderId": TestWxapi.api_orderId_id,
#             "remarkByCustomer": "傻逼",
#             "userId": TestWxapi.api_user_id
#
#
#         }
#
#         res = SendRequests().all_send_requests(method="post", url=url, json=datas, headers=headers)
#
#
# def loop_montor():
#     while True:
#         time_now = time.strftime("%H:%M:%S", time.localtime()) # 刷新
#         # print("当前时间", time_now)
#         if time_now == "11:29:00":
#             pytest.main(['-vs'])


if __name__ == '__main__':
    # loop_montor()
    pytest.main(['-vs'])