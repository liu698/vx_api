import json

import requests


# if res.status_code != 200:   # 推出程序
#     print('停止用例')
#     sys.exit()

# TestApi.api_token = res.json()['result']['token']  # 接口关联
# print('token_value:', TestApi.api_token)
from common.yaml_utli import read_yaml_testcase


class SendRequests:

    sess = requests.Session()

    def all_send_requests(self, **kwargs):

        print("\n")
        print("-----------------测试开始-----------------")
        res = SendRequests.sess.request(**kwargs)
        print('响应数据： ', res.json())
        print('响应时间', res.elapsed.total_seconds())
        assert res.elapsed.total_seconds() < 1
        print("-----------------测试结束-----------------")
        # assert res.status_code == 200
        return res

