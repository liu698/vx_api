from locust import HttpUser, task, between


def add(*args, **kwargs):
    print("add")


class Myuser(HttpUser):
    wait_time = between(1, 2)  # 等待时间
    # tasks = [add]
    @task
    def index_test(self):
        """访问首页测试"""
        self.client.get("http://api-test.haipingjia.cn/hpj-merchant/customer/area/getAllOpenCity.open")



# locust -f .\locustfile.py --web-host="127.0.0.1"


# web_ui:
# Number of users :模拟用户数
# Spawn rate ：用户增长速度