import os

import sys

from common.yaml_utli import clear_yaml

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import pytest



@pytest.fixture(scope='class', autouse=True)
def clear_session():

    clear_yaml()
    print("清空session")
    # yield
    # print("关闭数据库")
