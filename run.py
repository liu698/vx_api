import os
import time

import pytest

if __name__ == '__main__':
    pytest.main(['-vs', '--alluredir=./report/tmp'])
    time.sleep(3)
    os.system('allure serve ./report/tmp')